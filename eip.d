// Requires D Compiler v2.065


import std.stdio;
import std.digest.md;
import std.digest.digest;
import std.conv;
import std.string;

void main()
{
    MD5 md;
    md.start();
    for (int a=0; a < 256; ++a) {
        writeln("a: ", a);
        for (int b=0; b < 256; ++b)
            for (int c=0; c <256; ++c)
                for (int d=0; d < 256; ++d) {
                    string ip = to!string(a) ~ "." ~ to!string(b) ~ "." ~ to!string(c) ~ "." ~ to!string(d) ~ "\n";
                    md.put(cast(ubyte[])ip);
                }
    }

    ubyte[16] answer = md.finish();
    string str = toHexString!(LetterCase.lower)(answer);
    writeln("md5: ", str);
  
}
